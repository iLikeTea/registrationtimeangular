﻿

var routeModule = angular.module('appRegistration', ['ngRoute']);

routeModule.config(function ($routeProvider) {

    $routeProvider

        .when('/', {
            url: '/components/home',
            templateUrl: './components/home/home.view.html',
            controller: 'homeController'
        })

        .when('/listMantises', {
            url: '/components/listMantises',
            templateUrl: './components/listMantises/listMantises.view.html',
            controller: 'showMantisesController'
        })

        .when('/addMantis', {
            url: '/components/addMantis',
            templateUrl: './components/addMantis/addMantis.view.html',
            controller: 'addMantisController'
        })

        .otherwise({
            url: '/components/home',
            templateUrl: './components/home/home.view.html',
            controller: 'homeController'
        });
        
});







