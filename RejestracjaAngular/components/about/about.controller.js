﻿(function () {

    'use strict';

    angular.module('appRegistration')
        .controller('showMantisesController', function ($scope, $http) {

            $http({
                method: 'GET',
                url: 'http://sortia.medisoft.com.pl/api/Mantises'
            })
                .success(function (response) {
                    $scope.mantises = response;
                });

            $scope.clear = function () {
                if ($scope.searchMantis.Description.length == 0) {
                    delete $scope.searchMantis.Description;
                }
            }



        });

    //angular.module('appRegistration').factory('mantisFactory', function ($http) {
    //    return {
    //        getdata: function () {
    //            return $http.get('http://sortia.medisoft.com.pl/api/Mantises/4');
    //        }
    //    }
    //});

    //angular.module('appRegistration')
    //    .controller('showMantisesController', function ($scope, mantisFactory) {
    //        $scope.content = null;
    //        mantisFactory.getdata().success(function (data) {
            
    //            $scope.mantises = data[0];
    //        });

    //    });



})();