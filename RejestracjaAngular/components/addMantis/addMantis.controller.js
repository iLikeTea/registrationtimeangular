﻿(function () {

    'use strict';

    var app = angular.module('appRegistration');

    app.config(['$httpProvider', function ($httpProvider) {
        //Reset headers to avoid OPTIONS request (aka preflight)
        $httpProvider.defaults.headers.common = {};
        $httpProvider.defaults.headers.post = {};
        $httpProvider.defaults.headers.put = {};
        $httpProvider.defaults.headers.patch = {};
    }]);


    app.controller('addMantisController', function ($scope, $http) {

      

        $scope.mantis = {
            number: null,
            ID_Categories: null,
            is_Estimated: false,
            DateCreated:null,
            estimation: null,
            description: ''
        };
            
        $scope.resetMantisEstimation = function () {
            $scope.mantis.estimation = null;
        };
        
        $scope.projects = {};
        $scope.projects.id = null;

        $scope.projects.project = [
                { id: 8, name: "Strony" },
                { id: 7, name: "Support" },
                { id: 6, name: "Internal" },
                { id: 5, name: "Miniclaim" },
                { id: 4, name: "SzkodaApi" },
                { id: 3, name: "WebClaim" },
                { id: 2, name: "Audyt Bezpieczeństwa" }
        ];
        

        $scope.ButtonClick = function test() {
            var post = $http({
                method: 'POST',
                url: 'http://localhost:50564/api/Mantises',
                dataType: 'json',
                data: {
                    
                    MantisNo: $scope.mantis.number,
                    ID_Categories: 1,
                    //DateCreated: "2018-07-20T11:31:38.6367838+02:00",
                    is_Estimated:0,
                    Estimation: $scope.mantis.estimation,
                    Description: $scope.mantis.description
                },
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'

                }
            });

            post.success(function (data, status) {
                alert("sukces");
            });

            post.error(function (data, status) {
                 alert("porazka");
            });

       
        }

        //$scope.projects = {
                
        //    project: [
        //    { id: 8, name: "Strony" },
        //    { id: 7, name: "Support" },
        //    { id: 6, name: "Internal" },
        //    { id: 5, name: "Miniclaim" },
        //    { id: 4, name: "SzkodaApi" },
        //    { id: 3, name: "WebClaim" },
        //    { id: 2, name: "Audyt Bezpieczeństwa" }],

        //    selectedOption: { id: 6, name: "Internal"}               
        //}

    });

})();


