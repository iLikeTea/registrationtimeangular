﻿(function () {

    'use strict';

    var app = angular.module('appRegistration');

    app.service('utilities', function () {
        this.defaultIsEmpty = function (value, defaultValue) {
            return value == null ? defaultValue : value;
        }
    });

    app.controller('showMantisesController', function ($scope, $http, utilities) {

        $http({
            method: 'GET',
            url: 'http://localhost:50564/api/Mantises'
        })
            .success(function (response) {
                $scope.mantises = response;
            });

        $scope.clear = function () {
            if ($scope.searchMantis.Description.length == 0) {
                delete $scope.searchMantis.Description;
            }
        }

        $scope.checkEstimation = function (m) {
            return utilities.defaultIsEmpty(m.Estimation, '--')
        }

    });

 

    

    //angular.module('appRegistration').factory('mantisFactory', function ($http) {
    //    return {
    //        getdata: function () {
    //            return $http.get('http://sortia.medisoft.com.pl/api/Mantises/4');
    //        }
    //    }
    //});

    //angular.module('appRegistration')
    //    .controller('showMantisesController', function ($scope, mantisFactory) {
    //        $scope.content = null;
    //        mantisFactory.getdata().success(function (data) {
            
    //            $scope.mantises = data[0];
    //        });

    //    });



})();